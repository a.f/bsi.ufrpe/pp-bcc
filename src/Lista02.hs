module Lista02 where
import Data.List (nub)

type Pessoa = String
type Livro = String
type Emprestado = Bool
type Cadastro = (Pessoa, Livro, Emprestado)
type BancoDados = [Cadastro]

biblioteca :: BancoDados
biblioteca =
  [ ("", "The Cuckoo's Egg", False),
    ("", "The Cuckoo's Egg", False),
    ("", "Ghost in the Wires", False),
    ("", "Sandworm", False),
    ("", "Kingpin", False),
    ("Alice", "The Art of Cyberwarfare", True),
    ("", "Cult of the Dead Cow", False),
    ("", "Fatal System Error", False),
    ("Bob", "Countdown to Zero Day", True),
    ("", "Tracers in the Dark", False),
    ("", "Unmasked", False),
    ("Kevin", "This Machine Kills Secrets", True)
  ]

-- retorna os livros do acervo, sem repetição
-- livros :: BancoDados -> [Livro]
livros :: BancoDados -> [Livro]
livros [] = []
livros lib = nub [livro | (_, livro, _) <- lib]
-- EXEMPLO livros
-- $ livros biblioteca
-- $ ["The Cuckoo's Egg","Ghost in the Wires","Sandworm","Kingpin","The Art of Cyberwarfare","Cult of the Dead Cow","Fatal System Error","Countdown to Zero Day","Tracers in the Dark","Unmasked","This Machine Kills Secrets"]

-- retorna True se o livro está disponível
-- livroDisponivel :: BancoDados -> Livro -> Bool
livroDisponivel :: BancoDados -> Livro -> Emprestado
livroDisponivel [] _ = False
livroDisponivel ((_, livroBib, emprestado) : xs) livro
  | livroBib /= livro = livroDisponivel xs livro
  | livroBib == livro && not emprestado = True
  | otherwise = False
-- EXEMPLO livroDisponivel
-- $ livroDisponivel biblioteca "Unmasked"
-- $ True

-- retorna os livros que estao com uma pessoa
-- livrosPessoa :: BancoDados -> Pessoa -> [Livro]
livrosPessoa :: BancoDados -> Pessoa -> [Livro]
livrosPessoa [] _ = []
livrosPessoa lib client = [livro | (pessoa, livro, _) <- lib, client == pessoa]
-- EXEMPLO livrosPessoa
-- $ livrosPessoa biblioteca "Kevin"
-- $ ["This Machine Kills Secrets"]

-- retorna pares com nome do livro e a pessoa que está emprestado
-- emprestimos :: BancoDados -> [(Livro,Pessoa)]
emprestimos :: BancoDados -> [(Livro, Pessoa)]
emprestimos [] = []
emprestimos ((pessoa, livro, emprestado) : xs)
  | emprestado = (livro, pessoa) : emprestimos xs
  | otherwise = emprestimos xs
-- EXEMPLO emprestimos
-- $ emprestimos biblioteca
-- $ [("The Art of Cyberwarfare","Alice"),("Countdown to Zero Day","Bob"),("This Machine Kills Secrets","Kevin")]

-- adiciona emprestimo, se livro está disponível
-- emprestar :: BancoDados -> Pessoa -> Livro -> BancoDados
emprestar :: BancoDados -> Pessoa -> Livro -> BancoDados
emprestar [] _ _ = []
emprestar ((pessoaBib, livroBib, emprestado): xs) pessoa livro
  | livroBib == livro && not emprestado = (pessoa, livro, True) : emprestar xs pessoa livro
  | otherwise = (pessoaBib, livroBib, emprestado) : emprestar xs pessoa livro
-- EXEMPLO toLoan
-- $ toLoan biblioteca "Kevin" "Ghost in the Wires"
-- $ [("","The Cuckoo's Egg",False),("Kevin","Ghost in the Wires",True),("","Sandworm",False),("","Kingpin",False),("Alice","The Art of Cyberwarfare",True),("","Cult of the Dead Cow",False),("","Fatal System Error",False),("Bob","Countdown to Zero Day",True),("","Tracers in the Dark",False),("","Unmasked",False),("Kevin","This Machine Kills Secrets",True)]

-- Funcao que checa se o livro esta disponivel e salva o cadastro atualizado
empreste :: Cadastro -> Pessoa -> Livro -> Cadastro
empreste cadastro pessoa livro
  | livroBib == livro && not emprestado = (pessoa, livro, True)
  | otherwise = cadastro
  where
    (_, livroBib, emprestado) = cadastro
-- EXEMPLO empreste
-- $ empreste ("", "Sandworm", False) "Kevin" "Sandworm"
-- $ ("Kevin","Sandworm",True)

-- adiciona emprestimo, se livro está disponível
-- emprestar :: BancoDados -> Pessoa -> Livro -> BancoDados ||| VERSAO COM USO DE MAP |||
emprestarMap :: BancoDados -> Pessoa -> Livro -> BancoDados
emprestarMap [] _ _ = []
emprestarMap lib pessoa livro = map (\cadastro -> empreste cadastro pessoa livro) lib
-- EXEMPLO emprestarMap
-- $ emprestarMap biblioteca "Joana" "Unmasked"
-- $ [("","The Cuckoo's Egg",False),("","Ghost in the Wires",False),("","Sandworm",False),("","Kingpin",False),("Alice","The Art of Cyberwarfare",True),("","Cult of the Dead Cow",False),("","Fatal System Error",False),("Bob","Countdown to Zero Day",True),("","Tracers in the Dark",False),("Joana","Unmasked",True),("Kevin","This Machine Kills Secrets",True)]


-- devolve livre, se está emprestado
-- devolver :: BancoDados -> Pessoa -> Livro -> BancoDados
devolver :: BancoDados -> Pessoa -> Livro -> BancoDados
devolver [] _ _ = []
devolver ((pessoaBib, livroBib, emprestado): xs) pessoa livro
  | livroBib == livro && emprestado = ("", livro, False) : devolver xs pessoa livro
  | otherwise = (pessoaBib, livroBib, emprestado) : devolver xs pessoa livro
-- EXEMPLO toReturn
-- $ toReturn biblioteca "Kevin" "Ghost in the Wires"
-- $ [("","The Cuckoo's Egg",False),("","Ghost in the Wires",False),("","Sandworm",False),("","Kingpin",False),("Alice","The Art of Cyberwarfare",True),("","Cult of the Dead Cow",False),("","Fatal System Error",False),("Bob","Countdown to Zero Day",True),("","Tracers in the Dark",False),("","Unmasked",False),("Kevin","This Machine Kills Secrets",True)]
