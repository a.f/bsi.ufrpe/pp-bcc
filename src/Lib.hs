module Lib where

import System.Random

-- Int -> Finite integer
-- Integer -> Long integer

-- Factorial function utilizing Pattern Matching
-- Preferable than use of if <-> else
factorial :: Int -> Int
-- Calculate the factorial of a natural
factorial 0 = 1
factorial n = n * factorial (n - 1)

factorial3 :: Integer -> Integer
factorial3 n = product [0 .. n]

double :: Int -> Int
double x = x * 2

perimeter :: Int -> Int -> Int
perimeter width height = double (width + height)

xOr :: Bool -> Bool -> Bool
xOr a b = (a || b) && not (a && b)

factorial2 :: Integer -> Integer
factorial2 n = if n == 0 then 1 else n * factorial2 (n - 1)

-- "_" => anonymous variable

nand :: Bool -> Bool -> Bool
nand True True = False
nand _ _ = True

abs2 :: Int -> Int
-- return the absoluite value of an integer
abs2 n
  | n >= 0 = n
  | otherwise = -n

fastExp :: Integer -> Integer -> Integer
fastExp _ 0 = 1
fastExp x n =
  let y = fastExp x n_halved
      n_halved = div n 2
   in if even n
        then y * y
        else y * y * x

fastExp2 :: Integer -> Integer -> Integer
fastExp2 _ 0 = 1
fastExp2 x n
  | even n = y * y
  | otherwise = y * y * x
  where
    n_halved = div n 2
    y = fastExp2 x n_halved

power :: Integer -> Integer -> Integer
power n 0 = 1
power n m = n ^ m

power2 :: Integer -> Integer -> Integer
power2 m 0 = 1
power2 m n = m * power2 m (n - 1)

isPrime :: Int -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime n = not (hasDivisor (n - 1))
  where
    hasDivisor :: Int -> Bool
    hasDivisor 1 = False
    hasDivisor m = mod n m == 0 || hasDivisor (n - 1)

fibonacci :: Integer -> Integer
fibonacci n
  | n < 2 = n
  | otherwise = fibonacci (n - 1) + fibonacci (n - 2)

distance :: (Float, Float) -> (Float, Float) -> Float
distance p1 p2 = sqrt ((fst p1 - fst p2) ^ 2 + (snd p1 - snd p2) ^ 2)

distance2 :: (Float, Float) -> (Float, Float) -> Float
distance2 (n1, n2) (m1, m2) = sqrt ((n1 - m1) ^ 2 + (n2 - m2) ^ 2)

distance3 :: (Float, Float) -> (Float, Float) -> Float
distance3 p1 p2 = sqrt (sqr dx + sqr dy)
  where
    (x1, y1) = p1
    (x2, y2) = p2
    dx = x1 - x2
    dy = y1 - y2
    sqr x = x * x

maxi :: Integer -> Integer -> Integer
maxi m n
  | n >= m = n
  | otherwise = m

mini :: Integer -> Integer -> Integer
mini m n
  | n <= m = n
  | otherwise = m

maior :: Integer -> Integer -> Integer -> Integer
maior a b c = maxi a (maxi b c)

menor :: Integer -> Integer -> Integer -> Integer
menor a b c = mini a (mini b c)

vendas :: Int -> Int
vendas n
  | n == 0 = 20
  | n == 1 = 50
  | n == 2 = 30
  | n == 3 = 50
  | otherwise = 2 * n

vendas2 :: Int -> Int
vendas2 0 = 20
vendas2 1 = 50
vendas2 2 = 30
vendas2 3 = 50
vendas2 n = 2 * n

totalVendas :: Int -> Int
totalVendas n
  | n == 0 = vendas 0
  | otherwise = totalVendas (n - 1) + vendas n

totalVendas2 :: Int -> Int
totalVendas2 0 = vendas 0
totalVendas2 n = totalVendas2 (n - 1) + vendas n

addSpaces :: Int -> String
addSpaces n
  | n == 0 = ""
  | otherwise = " " ++ addSpaces (n - 1)

addSpaces2 :: Int -> String
addSpaces2 0 = ""
addSpaces2 n = " " ++ addSpaces (n - 1)

forRight :: Int -> String -> String
forRight 0 str = str
forRight n str = addSpaces n ++ str

sumList :: [Integer] -> Integer
sumList [] = 0
sumList (x : xs) = x + sumList xs

sumList2 :: [Integer] -> Integer
sumList2 list =
  case list of
    [] -> 0
    x : xs -> x + sumList2 xs

doubleInt :: [Int] -> [Int]
doubleInt [] = []
doubleInt (first : rest) = first ^ 2 : doubleInt rest

isMember :: Integer -> [Integer] -> Bool
isMember n [] = False
isMember n (x : xs) = (x == n) || isMember n xs

isMember2 :: Integer -> [Integer] -> Bool
isMember2 n [] = False
isMember2 n (x : xs)
  | x == n = True
  | otherwise = isMember2 n xs

maximo :: [Int] -> Int
maximo [] = minBound :: Int
maximo [x] = x
maximo (x : xs)
  | x > maximo xs = x
  | otherwise = maximo xs
