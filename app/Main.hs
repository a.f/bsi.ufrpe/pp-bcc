module Main (main) where

-- import Lib
import Lista02

main :: IO ()
main = do
  -- print (factorial 5)
  -- print (factorial2 5)
  -- print (double 2)
  -- print (perimeter 2 3)
  -- print (fastExp 2 10)
  -- print ("Power " ++ show (power (-2) 7))
  -- print ("Power2 " ++ show (power2 (-2) 7))
  -- print ("Fibonacci " ++ show (fibonacci 6))
  -- print (maior 7 20 (-10))
  -- print (menor 7 20 (-12))
  -- print (vendas 5)
  -- print (vendas2 5)
  -- print (totalVendas 5)
  -- print (totalVendas2 5)
  -- let a = addSpaces 5
  -- let b = addSpaces2 5
  -- print a
  -- print ("# spaces = " ++ show (length a))
  -- print b
  -- print ("# spaces = " ++ show (length b))
  -- print ("ParaDireita:" ++ forRight 3 "Teste")
  -- print ("isMember " ++ show (isMember 3 [0 .. 10]))
  -- print ("isMember " ++ show (isMember 3 [10 .. 100]))
  -- print ("isMember2 " ++ show (isMember2 3 [0 .. 10]))
  -- print ("isMember2 " ++ show (isMember2 3 [10 .. 100]))
  -- print ("Maximo " ++ show (maximo [0 .. 20]))
  -- print "FIM"

  -- LISTA 02 --
  print (livros biblioteca)
  print (livroDisponivel biblioteca "Unmasked")
  print (livroDisponivel biblioteca "The Art of Cyberwarfare")
  print (livrosPessoa biblioteca "Kevin")
  print (emprestimos biblioteca)
  print (emprestar biblioteca "Kevin" "Ghost in the Wires")
  print (empreste ("", "Sandworm", False) "Kevin" "Sandworm")
  print (emprestarMap biblioteca "Joana" "Unmasked")
  print (devolver biblioteca "Kevin" "Ghost in the Wires")
